<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $binatang = new Animal("shaun");
    echo "Nama Binatang   : ".$binatang->get_name."<br>"; // "shaun"
    echo "Jumlah kaki     : ".$binatang->get_leg."<br>"; // 4
    echo "Berdarah dingin : ".$binatang->get_cold_blooded."<br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "Nama Binatang   : ".$sungokong->get_name."<br>";  
    echo "Jumlah kaki     : ".$sungokong->get_leg."<br>";  
    echo "Berdarah dingin : ".$sungokong->get_cold_blooded."<br>";  
    echo $sungokong->suara()."<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama Binatang   : ".$kodok->get_name."<br>";  
    echo "Jumlah kaki     : ".$kodok->get_leg."<br>";  
    echo "Berdarah dingin : ".$kodok->get_cold_blooded."<br>";  
    echo $kodok->Jump()."<br><br>";

?>